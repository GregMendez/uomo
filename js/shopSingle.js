var description = document.getElementById("main__additionnal__infos__description__id");
var additionnal = document.getElementById("main__additionnal__infos__additionnal__id");
var reviews = document.getElementById("main__additionnal__infos__reviews__id");
var descriptionBtn = document.getElementById("additionnal__infos__links__description__id");
var additionnalBtn = document.getElementById("additionnal__infos__links__infos__id");
var reviewsBtn = document.getElementById("additionnal__infos__links__reviews__id");
var relatedProduct = document.getElementById('related__product__id');
var bigImg = document.getElementById("bigImg");

function showDescription() {
  description.style.visibility = "visible";
  additionnal.style.visibility = "hidden";
  reviews.style.visibility = "hidden";

  descriptionBtn.style.color = "#222222";
  additionnalBtn.style.color = "#767676";
  reviewsBtn.style.color = "#767676";

  descriptionBtn.style.borderBottom = "3px solid #222222";
  additionnalBtn.style.borderBottom = "none";
  reviewsBtn.style.borderBottom = "none";

  //relatedProduct.style.marginTop = "-600px";
  description.style.height = "500px";
  additionnal.style.height = "0";
  reviews.style.height ="0";
}

function showAdditionnalInfo(){

  description.style.visibility = "hidden";
  additionnal.style.visibility = "visible";
  reviews.style.visibility = "hidden";

  descriptionBtn.style.color = "#767676";
  additionnalBtn.style.color = "#222222";
  reviewsBtn.style.color = "#767676";

  additionnalBtn.style.borderBottom = "3px solid #222222";
  descriptionBtn.style.borderBottom = "none";
  reviewsBtn.style.borderBottom = "none";

  additionnal.style.height = "100px";
  description.style.height = "0";
  reviews.style.height ="0";
  //relatedProduct.style.marginTop = "-800px";
}

function showReviews() {
  description.style.visibility = "hidden";
  additionnal.style.visibility = "hidden";
  reviews.style.visibility = "visible";

  descriptionBtn.style.color = "#767676";
  additionnalBtn.style.color = "#767676";
  reviewsBtn.style.color = "#222222";

  descriptionBtn.style.borderBottom = "none";
  additionnalBtn.style.borderBottom = "none";
  reviewsBtn.style.borderBottom = "3px solid #222222";

  //relatedProduct.style.marginTop = "0px";
  additionnal.style.height = "0";
  description.style.height = "0";
  reviews.style.height ="800px";
}

function showFirstImg(){
  var img = document.getElementById("first__img");
  let src = img.src;

  bigImg.src = src;
}

function showSecondImg(){
  var img = document.getElementById("second__img");
  let src = img.src;

  bigImg.src = src;
}

function showThirdImg(){
  var img = document.getElementById("third__img");
  let src = img.src;

  bigImg.src = src;
}

function showFourthImg(){
  var img = document.getElementById("fourth__img");
  let src = img.src;

  bigImg.src = src;
}
