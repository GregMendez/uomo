// Initialize and add the map
function initMap() {
  // The location of Uluru
  const geneva = { lat: 46.20589015706231, lng: 6.140731303031445 };
  const newYorkStore = { lat: 46.204421373153664, lng: 6.09665993762553 };
  const istanbulStore = { lat: 46.201237267636046, lng: 6.134503249562987 };
  const londonStore = { lat: 46.17383788733115, lng: 6.133351993833973 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 10,
    center: geneva,
  });
  // The marker, positioned at Uluru
  const newYorkMarker = new google.maps.Marker({
    position: newYorkStore,
    map: map,
  });

  const istanbulMarker = new google.maps.Marker({
    position: istanbulStore,
    map: map,
  });

  const londonMarker = new google.maps.Marker({
    position: londonStore,
    map: map,
  });
}
