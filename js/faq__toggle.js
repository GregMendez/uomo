let orderFirst = document.getElementById('order__first');
let orderSecond = document.getElementById('order__second');
let orderThird = document.getElementById('order__third');

let shippingFirst = document.getElementById('shipping__first');
let shippingSecond = document.getElementById('shipping__second');
let shippingThird = document.getElementById('shipping__third');

let paymentFirst = document.getElementById('payment__first');
let paymentSecond = document.getElementById('payment__second');
let paymentThird = document.getElementById('payment__third');

function toggleContent(subtitle, content, text) {
  if(content.style.visibility == "visible") {
    content.style.visibility = "hidden";
    content.style.marginTop = "0px";
    content.style.marginBottom = "0px";
    text.textContent = "+";
    subtitle.style.borderColor = "#E4E4E4";
  }
  else {
    content.style.visibility = "visible";
    content.style.marginTop = "30px";
    content.style.marginBottom = "120px";
    text.textContent = "-";
    subtitle.style.borderColor = "#222222";
  }
}

orderFirst.onclick = function () {
  let content = document.getElementById("order__first__content");
  let text = orderFirst.children[1];
  toggleContent(orderFirst, content, text);
}

orderSecond.onclick = function () {
  let content = document.getElementById("order__second__content");
  let text = orderSecond.children[1];
  toggleContent(orderSecond, content, text);
}

orderThird.onclick = function () {
  let content = document.getElementById("order__third__content");
  let text = orderThird.children[1];
  toggleContent(orderThird, content, text);
}

shippingFirst.onclick = function () {
  let content = document.getElementById("shipping__first__content");
  let text = shippingFirst.children[1];
  toggleContent(shippingFirst, content, text);
}

shippingSecond.onclick = function () {
  let content = document.getElementById("shipping__second__content");
  let text = shippingSecond.children[1];
  toggleContent(shippingSecond, content, text);
}

shippingThird.onclick = function () {
  let content = document.getElementById("shipping__third__content");
  let text = shippingThird.children[1];
  toggleContent(shippingThird, content, text);
}

paymentFirst.onclick = function () {
  let content = document.getElementById("payment__first__content");
  let text = paymentFirst.children[1];
  toggleContent(paymentFirst, content, text);
}

paymentSecond.onclick = function () {
  let content = document.getElementById("payment__second__content");
  let text = paymentSecond.children[1];
  toggleContent(paymentSecond, content, text);
}

paymentThird.onclick = function () {
  let content = document.getElementById("payment__third__content");
  let text = paymentThird.children[1];
  toggleContent(paymentThird, content, text);
}
